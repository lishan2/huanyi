/**
 * api接口统一管理
 */
import { get, post } from './http'

// 轮播图列表
export const imgList = p => get('/business/image/list', p);
//创新-学生作品
export const workList = p => get('/innovate/works/list', p);
//创新-联合教学
export const   educationList = p => get('/innovate/education/list', p);
//创新-实践成果
export const practiceList = p => get('/innovate/practice/list', p);

 //创业流程
export const flowList = p => get('/enterprise/flow/list', p);
 //创业合作
export const cooperationList = p => get('/enterprise/cooperation/list', p);
 //地方服务
export const servicesList = p => get('/enterprise/services/list', p);

 //私信
export const messageApi = p => get('/business/letter/save', p);

 //学生作品详情
export const detailsApi = p => get('/business/data/details', p);
 //公告信息列表
export const commonApi = p => get('/enterprise/affiche/list', p);
