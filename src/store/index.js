import Vue from 'vue'
import Vuex from 'vuex'
 Vue.use(Vuex)
 const store = new Vuex.Store({
   state: {
   	token:'',
   },
   mutations: {
   	// 保存用户登录信息
   	setToken(state, provider) {
   		// sessionStorage.setItem('token', JSON.stringify(provider))
   		state.token = provider;
   	},
   },
   actions: {
   	updateToken(context, value) {
   		context.commit('setToken', value)
   	},
   }
 });

export default store
