import Vue from 'vue'
import Router from 'vue-router'
// import home from '@/views/home'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: (resolve) => require(['../views/home/index.vue'], resolve),
    },
    {
        path: '/innovation',
        component: (resolve) => require(['../views/innovation/index.vue'], resolve),
        children: [
            // 作品展示
            // {
            //     path: '/innovation/work',
            //     component: (resolve) => require(['../views/innovation/work'], resolve),
            // },
        ],
    },
     {
        path: '/entrepreneurship',
        component: (resolve) => require(['../views/entrepreneurship/index.vue'], resolve),
        children: [
            // 创业流程
            // {
            //     path: '/entrepreneurship/process',
            //     component: (resolve) => require(['../views/entrepreneurship/process'], resolve),
            // }
        ],
    },
    //作品详情
    {
      path: '/workDetail',
      name: 'workDetail',
      component: (resolve) => require(['../views/workDetail/index'], resolve),
    },
    //私信我们
    {
      path: '/message',
      name: 'message',
      component: (resolve) => require(['../views/message/index'], resolve),
    },
  ]
})
